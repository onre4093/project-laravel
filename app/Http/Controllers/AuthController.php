<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function welcome(Request $request){
        $fname = $request->fname;
        return view('halaman.welcome', compact('fname'));
    }
}
